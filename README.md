# ansible-role-containerd

Install containerd and these components: runc, cni.

## Usage

Modify variables as you want with yours needs, located in `defaults/main.yml`

```
containerd_version: "1.7.2"
containerd_default_config: true
containerd_config_cgroup_systemd: true

runc: true
runc_version: "1.1.7"

cni: true
cni_version: "1.3.0"
```

## Authors and acknowledgment
Role created by Evan BITIC

## License
MIT